import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FooProvider } from "../../providers/providers";
import { PushPopDetailPage } from "../push-pop-detail/push-pop-detail";

/**
 * Generated class for the PushPopPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-push-pop',
  templateUrl: 'push-pop.html',
})
export class PushPopPage {

  me: any = this;
  data: any = [];
  pushPopDetail: any = PushPopDetailPage;

  constructor(public fooService:FooProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    
     this.fooService.get()
      .subscribe(
        data => { this.data = data.json(); console.log(this.data); },
        err => console.log(err),
        () => console.log("Users Loaded!")
      );

  }

}
