import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PushPopDetailPage } from './push-pop-detail';

@NgModule({
  declarations: [
    PushPopDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(PushPopDetailPage),
  ],
})
export class PushPopDetailPageModule {}
