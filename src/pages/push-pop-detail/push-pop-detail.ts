import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FooProvider } from "../../providers/providers";

/**
 * Generated class for the PushPopDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-push-pop-detail',
  templateUrl: 'push-pop-detail.html',
})
export class PushPopDetailPage {

  id: any;
  detail: any = {};
  title: any = "";

  constructor(public fooService:FooProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.id = this.navParams.data;
  }

  ionViewDidLoad() {
    this.fooService.getById(this.id)
    .subscribe(
        data => { this.detail = data.json(); console.log(this.detail); this.title = this.detail.name; },
        err => console.log(err),
        () => console.log("Users Loaded!")
      );
  }

}
